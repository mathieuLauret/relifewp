<?php
function my_enqueue_assets()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css', array(), time());
}
add_action('wp_enqueue_scripts', 'my_enqueue_assets');

function my_enqueue_sass()
{
    wp_enqueue_style('style-css', get_stylesheet_directory_uri() . '/style-min.css', array(), time());
}
add_action('wp_enqueue_scripts', 'my_enqueue_sass');
?>