const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
  entry: [
    //  js: './path/to/my/entry/file.js',
    "./scss/style.scss",
  ],

  output: {
    path: path.resolve(__dirname),
  },

  mode: "production",

  watch: true,

  module: {
    rules: [
      {
        test: /js\/\ext.*\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader",
          {
            loader: "sass-resources-loader",
            options: {
              resources: [
                path.resolve(__dirname, "./scss/_variables.scss"),
                path.resolve(__dirname, "./scss/mixins/_breakpoints.scss"),
                path.resolve(__dirname, "./scss/mixins/_mediaqueries.scss"),
              ],
            },
          },
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              outputPath: "img",
            },
          },
        ],
      }, 
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
            }
          }
        ]
      }
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "style-min.css",
    }),
  ],
  optimization: {
    minimizer: [new OptimizeCSSAssetsPlugin({
      cssProcessorPluginOptions: {
        preset: ['default', { discardComments: { removeAll: false } }],
      }
    })],
  },
};
